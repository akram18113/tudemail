#!/usr/bin/python3
import os
import sqlite3
import data_store
from flask import Flask, render_template, request, redirect
from flask_mail import Mail, Message
import pandas as pd
from form_contact import ContactForm, csrf
from googletrans import Translator


import sqlite3 as sql

mail = Mail()

app = Flask(__name__)

SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

csrf.init_app(app)
app.config.update(
    TESTING=True,
    SECRET_KEY='192b9bdd22ab9ed4d12e236c78afcb9a393ec15f71bbf5dc987d54727823bcbf'
)

mail.init_app(app)

csrf.init_app(app)


@app.route('/')
def index():
    return render_template('views/home/index.html')

@app.route('/unsubscribe', methods=['POST', 'GET'])
def contact():
    form = ContactForm()
    if form.validate_on_submit():        
        print('-------------------------')
        nm = request.form['name']
        print(nm)
        addr = request.form['email']
        print(addr)      
        print('-------------------------')
        send_message(request.form)
        with sql.connect("database.db") as con:
            cur = con.cursor()
            
            cur.execute("INSERT INTO unsubscribe(name,addr) VALUES (?,?)",(nm,addr) )
            con.commit()
            msg = "Record successfully added"
            db_df = pd.read_sql_query("SELECT * FROM unsubscribe",con)
            db_df.to_csv('shah.csv', index=False)
        return redirect('/list')    

    return render_template('views/contacts/unsubscribe.html', form=form)

@app.route('/list')
def list():
    con = sql.connect("database.db")
    con.row_factory = sql.Row
   
    cur = con.cursor()
    cur.execute("select * from unsubscribe")
   
    rows = cur.fetchall();
    return render_template('views/home/list.html',rows = rows)


def send_message(message):
    print(message.get('name'))

    msg = Message(message.get('subject'), sender = message.get('email'),
            recipients = ['id1@gmail.com'],
            body= message.get('message')
    )  
    mail.send(msg)

if __name__ == "__main__":
    app.run(debug = True)