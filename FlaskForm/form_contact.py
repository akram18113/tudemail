from flask_wtf import FlaskForm, CSRFProtect
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import DataRequired, Email

csrf = CSRFProtect()

class ContactForm(FlaskForm):
    name = StringField('Vor- und Nachname', validators=[DataRequired('Bitte geben Sie Ihren Namen ein')])
    email = StringField('E-mail', validators=[DataRequired('Geben Sie die E-Mail ein, die Sie abbestellen möchten'),Email('Geben Sie eine gültige E-Mail ein')])
    submit = SubmitField("Einreichen")