import smtplib

from string import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import datetime
import wikiquote
from datetime import datetime
import json





def main():
    names = ['Akram'] 
    emails  = ['abdullahakram2000@gmail.com']
    message_template = ['message.txt']

    # set up the SMTP server
    s = smtplib.SMTP(host='smtp.gmail.com', port=587)
    s.starttls()
    s.login('GMAIL_EMAIL', 'GMAIL_PASSWORD')

    # For each contact, send the email:
    for name, email in zip(names, emails):
        msg = MIMEMultipart()       # create a message

        # add in the actual person name to the message template

        message = message_template.substitute(PERSON_NAME=name.title(),   QUOTE=wikiquote.qotd()[0])

        # Prints out the message body for our sake
        print(message)

        # setup the parameters of the message
        msg['From']=name
        msg['To']=email
        msg['Subject']="This is TEST"
        
        # add in the message body
        msg.attach(MIMEText(message, 'plain'))
        
        # send the message via the server set up earlier.
        s.send_message(msg)
        del msg
        
    # Terminate the SMTP session and close the connection
    s.quit()

    
myFile = open('append.json', 'a') 
data = {
    "glossary": {
        "status": [myFile.write('\nAccessed on ' + str(datetime.now()))],
        "message_file": ["message.txt"],

        "tasks": {
            "id": ["1"],
            "address_file": ["mycontacts.txt"],
            "status": ["done"]
        }
    }
}

json.dump(data, myFile)


if __name__ == '__main__':
    main()