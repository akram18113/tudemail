#!/usr/bin/env python
# -*- coding: utf-8 -*-
import email, smtplib, ssl

from string import Template
from email.header import Header
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate, ake_msgid
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
import imghdr
from email.message import EmailMessage
import os
from email.headerregistry import Address


PASSWORD = 'mypassword'

def get_contacts(filename):
  
    emails = []
    with open(filename, 'r') as contacts_file:
        for a_contact in contacts_file:
            #print a_contact
            emails.append(a_contact)
    return emails

def read_template(filename):
    
    with open(filename, 'r') as template_file:
        template_file_content = [template_file.read()]
    return template_file_content

def fileattach(filename):
    
    filename = "document.pdf"  # In same directory as script

    # Open PDF file in binary mode
    with open(filename, "rb") as attachment:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())

    # Encode file in ASCII characters to send by email    
    encoders.encode_base64(part)

    # Add header as key/value pair to attachment part
    part.add_header(
        "Content-Disposition",
        f"attachment; filename= {filename}",
    )
    return part


def main():
    emails = get_contacts("./mycontacts.txt") # read contacts file
    filename = "./message.txt"
    message = read_template(filename)

    #print message
    
    # set up the SMTP server
    #s = smtplib.SMTP(host='smtp.host', port=25)
    #s.starttls()
    #s.login(MY_ADDRESS,PASSWORD)

    # For each contact, send the email:
    for email in emails:
        s = smtplib.SMTP(host='smtp.host', port=25)
        s.starttls()
     
        #print email
        body = ' '.join(msg)
        msg = MIMEText(body)


        
        # setup the parameters of the message
        msg['From']=MY_ADDRESS
        msg['To']=email
        msg['Subject']='Subject'
        msg.add_alternative("""\
            <!DOCTYPE html>
            <html>
                <body>
                    <h1 style="color:SlateGray;">This is an HTML Email!</h1>
                </body>
            </html>
            """, subtype='html')

        
        # add in the message body
        #msg.attach(MIMEText(message, 'plain', 'utf-8'))
        try:
            s.sendmail(MY_ADDRESS, email, msg.as_string())
        except:
            print(email)
                
        del msg
        
        s.quit()
    
if __name__ == '__main__':
    main()
