#!/usr/bin/env python
# -*- coding: utf-8 -*-
import smtplib

from string import Template

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header


MY_ADDRESS = 'sifa-support@mailbox.tu-dresden.de'
PASSWORD = ''

def get_contacts(filename):
  
    emails = []
    with open(filename, 'r') as contacts_file:
        for a_contact in contacts_file:
            #print a_contact
            emails.append(a_contact)
    return emails

def read_template(filename):
    
    with open(filename, 'r') as template_file:
        template_file_content = [template_file.read()]
    return template_file_content

def main():
    emails = get_contacts("./test_mycontacts.txt") # read contacts file
    filename = "./message.txt"
    message = read_template(filename)

    #print message
    
    # set up the SMTP server
    #s = smtplib.SMTP(host='smtp.host', port=25)
    #s.starttls()
    #s.login(MY_ADDRESS,PASSWORD)

    # For each contact, send the email:
    for email in emails:
        s = smtplib.SMTP(host='mailoutmm.zih.tu-dresden.de', port=25)
        s.starttls()

        #print email
        body = ' '.join(message)
        msg = MIMEText(body)      
        
        # setup the parameters of the message
        msg['From']=MY_ADDRESS
        msg['To']=email
        msg['Subject']='Testmail'
        
        # add in the message body
        #msg.attach(MIMEText(message, 'plain', 'utf-8'))
        try:
            s.sendmail(MY_ADDRESS, email, msg.as_string())
        except:
            print(email)
                
        del msg
        
        s.quit()
    
if __name__ == '__main__':
    main()
